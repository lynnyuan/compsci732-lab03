import { Container, makeStyles, Typography } from '@material-ui/core';
import { useContext } from 'react';
import { AppContext } from '../AppContextProvider';
import { Offline, Online } from 'react-detect-offline';
import useOnlineStatus from '@rehooks/online-status';

const useStyles = makeStyles((theme) => ({
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6, 0),
    },
    online: {
        color: 'green'
    },
    offline: {
        color: 'red'
    }
}));

export default function Footer({ title, description }) {
    const isOnline = useOnlineStatus();

    const classes = useStyles();

    // const isOnline = true;
    console.log(isOnline)

    return (
        <footer className={classes.footer}>
            <Container maxWidth="lg">
                <hr />
                <Typography variant="h6" align="center" gutterBottom>
                <div>
                    <Online>Only shown when you're online</Online>
                    <Offline>Only shown offline (surprise!)</Offline>
                </div>
                    {title}
                    <em> (currently </em>
                    {isOnline ? (
                        <em className={classes.online}>online</em>
                    ) : (
                        <em className={classes.offline}>offline</em>
                    )}
                    <em>)</em>
                </Typography>
                <Typography
                    variant="subtitle1"
                    align="center"
                    color="textSecondary"
                    component="p"
                >
                    {description}
                </Typography>
            </Container>
        </footer>
    )
}