import { Detector } from "react-detect-offline";
import { useState } from 'react';

export default function useDetect(){
    
   const[isOnline, setisOnline] = useState({})

   function detect(){
      return(
         <Detector
            onChange ={({ online }) => (
               setisOnline({
                  isOnline: online
               })
            )}
         />
      )
   }
   console.log(isOnline)
   return isOnline
}